var Stack = function(params) {
  console.log(params);
  var svg,
    initialized = false,
    data = params.data,
    el = params.el,
    margin = { top: 30, right: 20, bottom: 40, left: 130 },
    elWidth = d3
      .select(el)
      .node()
      .getBoundingClientRect().width,
    width = Math.min(elWidth, 950) - margin.left - margin.right,
    height = 650 - margin.top - margin.bottom;

  function replaceChar(str) {
    var name = str
      .replace(/\s+/g, "")
      .replace("'", "")
      .replace("/", "");
    return name;
  }

  function setData() {
    if (initialized) update();
    else initStack();
  }

  function initStack() {
    initialized = true;
  }

  function update() {}

  function init() {
    setData();
  }
  return {
    init: init,
    setData: setData
  };
};
