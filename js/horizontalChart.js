// .paddingOuter(1);
//create x axisvar
// xAxis = d3.axisBottom().scale(xScale);

//load csv
// d3.csv("CO2-Emissions.csv", function(error, data){
function horizontalChart(el, data, field, title) {
  //iterate throught data to convert strings to numbers
  // data.forEach(function(d) {
  //   d.str = d.values.length;
  //   console.log(d.str);
  //   // d.values.length = +d.values.length || +d.value;
  // });

  var margin = { top: 30, right: 20, bottom: 40, left: 130 },
    elWidth = d3
      .select(el)
      .node()
      .getBoundingClientRect().width,
    width = Math.min(elWidth, 950) - margin.left - margin.right,
    height = 650 - margin.top - margin.bottom;

  // Adjust in case of small devices
  var isMobile = width < 500;

  if (isMobile) {
    margin.left = 115;
  }

  //create the x scale
  var xScale = d3.scaleLinear();

  //create the y scale
  var yScale = d3
    .scaleBand()
    .rangeRound([0, height], 0.1)
    .paddingInner(0.1);
  d3
    .select(el)
    .append("h1")
    .html(title);

  //set xScale domain
  xScale.range([0, width]).domain(
    d3.extent(data, function(d) {
      return d.values.length;
    })
  );

  //set yScale domain
  yScale //.domain(d3.range(data.length));
    .domain(
      data
        .map(function(d) {
          return d.key;
        })
        .reverse()
    );
  //create the container
  var svg = d3
    .select(el)
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

  //creating x Axis
  svg
    .append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(" + margin.left + "," + height + ")");
  // .call(xAxis);

  //select bars
  var bar = svg
    .append("g")
    .attr("class", "bars")
    .attr("transform", "translate(" + margin.left + ",0)")
    .selectAll(".bar")
    .data(data);

  //create bar elements
  bar
    .enter()
    .append("rect")
    .attr("width", function(d) {
      return xScale(d.values.length);
    })
    .attr("height", yScale.bandwidth())
    .attr("x", 0)
    .attr("y", function(d, i) {
      return yScale(d.key);
    })
    .attr("fill", "darkred")
    .attr("class", "bar");
  // .append("title")
  // .html(function(d) {
  //   return d[field] + ": " + d.values.length;
  // });

  //select text bar values
  var values = svg
    .append("g")
    .attr("class", "values")
    .attr("transform", "translate(" + margin.left + ",0)")
    .selectAll(".value")
    .data(data);

  //create text elements
  values
    .enter()
    .append("text")
    .attr("class", "value")
    .attr("x", function(d) {
      return xScale(d.values.length);
    })
    .attr("dy", ".45em")
    .attr("dx", ".5em")
    .attr("y", function(d, i) {
      return yScale(d.key) + yScale.bandwidth() / 2;
    })
    .text(function(d) {
      // return addComas(d.values.length);
      return d.values.length;
    });

  // d3.selectAll(".values").attr("transform", "translate(" + margin.left + ",0)");

  //create companies texts
  var companies = svg
    .append("g")
    .attr("class", "companies")
    .attr("transform", "translate(" + margin.left + ", 0)")
    .selectAll(".country")
    .data(data)
    .enter()
    .append("text")
    .attr("class", "country")
    .attr("x", 0)
    .attr("dy", ".45em")
    .attr("y", function(d, i) {
      return yScale(d.key) + yScale.bandwidth() / 2;
    })
    .tspans(function(d) {
      return d3.wordwrap(d[field], 15); // break line after 15 characters
    })
    .attr("text-anchor", "end")
    // .text(function(d) {
    //   return d[field];
    // })
    .attr("dx", "-1rem");

  // create the line between companies and bars
  var line = svg
    .append("line")
    .attr("x1", 0)
    .attr("x2", 0)
    .attr("y1", 0)
    .attr("y2", yScale(data[0].key) + yScale.bandwidth())
    .attr("stroke", "#777")
    .attr("stroke-width", 1)
    .attr("transform", "translate(" + margin.left + ",0)");

  //create x axis animation from zero
  // d3
  //   .selectAll(".x.axis")
  //   .transition()
  //   .duration(1000)
  //   .call(xAxis);

  // });
}
