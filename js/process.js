var url = "data/map_resources.json";
d3.json(url, function(error, data) {
  window.data = data;
  console.log(data);
  var catById = [];

  // Create a Objects accessible by 'primary_category_id'
  data.categories.forEach(function(d) {
    var id = String(d.id);
    catById[id] = d.title;
  });

  // Array of categories String
  var categories = data.categories.map(function(d) {
    return d.title;
  });

  // Has map function to count by a given field
  // function countCategory(arr, field) {
  //   return arr.reduce(function(result, company) {
  //     if (!result[catById[company[field]]]) {
  //       result[catById[company[field]]] = 0;
  //     }
  //     result[catById[company[field]]] += 1;
  //     return result;
  //   }, {});
  // }

  // Nesting by typology
  var byTypology = d3
    .nest()
    .key(function(d) {
      return d.typology;
    })
    .entries(data.map_resources)
    .sort(function(a, b) {
      return d3.ascending(a.values.length, b.values.length);
    });

  // var byCategoryId = countCategory(data.map_resources, "primary_category_id");
  var byCategoryId = d3
    .nest()
    .key(function(d) {
      return catById[d.primary_category_id];
    })
    .entries(data.map_resources)
    .sort(function(a, b) {
      return d3.ascending(a.values.length, b.values.length);
    });

  console.log(byCategoryId);
  horizontalChart("#graphs", byTypology, "key", "Recursos por tipología");
  horizontalChart("#graphs", byCategoryId, "key", "Startups según categoría");

  var stack = new Stack({
    el: "#graphs",
    data: byCategoryId,
    field: "key",
    title: "Startups según categoría"
  });

  stack.init();
});
